//
//  DetailViewConrtoller.swift
//  Project1
//
//  Created by PRO13 on 16.11.2020.
//

import UIKit

class DetailViewConrtoller: UIViewController {

    @IBOutlet var ImageView: UIImageView!
    var selectedImage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = selectedImage
        navigationItem.largeTitleDisplayMode = .never

        if let imageLoad = selectedImage {
            ImageView.image = UIImage(named: imageLoad)
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }
}
